#!/usr/bin/python3

import requests
import os
from os.path import exists, join
import sys
import concurrent.futures
import tempfile
import re
from datetime import datetime
import argparse


temp_filename = "temp%05d.ts"

def download_single_part(i):
	if exists(temp_filename % i):
		return
	try:
		url = base_url % i
		r = requests.get(url)

		file = open(join(dirname, temp_filename % i), "wb")
		file.write(r.content)
		file.close()
		return True
	except:
		print("error downloading %d" % i)
		return False

def find_missing_parts():
	def get_index(filename):
		return int(re.match("temp(.*)\.ts", filename).groups()[0])

	last_downloaded_part = get_index(sorted(os.listdir(dirname))[-1])

	downloaded_files = [get_index(filename) for filename in os.listdir(dirname)]
	return list(set(range(last_downloaded_part)) - set(downloaded_files))

def download_all_parts():
	print("[+] Starting to download the video")
	download_parts(range(1500))

	print("[+] Retrying to download some missing parts")
	download_parts(find_missing_parts())

def download_parts(parts):
	with concurrent.futures.ThreadPoolExecutor(max_workers = 10) as executor:
		future_to_url = {executor.submit(download_single_part, i): i for i in parts}
		for future in concurrent.futures.as_completed(future_to_url):
			i = future_to_url[future]
			print(i)


def convert_to_mp4(output_filename, downloads_dir="downloads"):
	if not os.path.exists(downloads_dir):
		os.makedirs(downloads_dir)

	os.system("cat `ls {0}/*.ts | sort` > {0}/all.ts".format(dirname))
	os.system("ffmpeg -i {0}/all.ts -acodec copy -vcodec copy {1}/{2}".format(dirname, downloads_dir, output_filename))
	os.system("rm -rf *.ts")


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('url', type=str, action='store', help='an integer for the accumulator')
	parser.add_argument('--output-filename', action='store', help='sum the integers (default: find the max)')

	args = parser.parse_args()

	if args.output_filename is None:
		outputfilename = "lesson-" + datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + ".mp4"
	else:
		outputfilename = args.output_filename


	last_slash = args.url.rfind("/")
	base_url = args.url[:last_slash+1] + "%05d.ts"
	
	with tempfile.TemporaryDirectory(dir=".") as tempdir:
		print("[+] Created a temp dir at: %s" % tempdir)
		dirname = tempdir
		download_all_parts()

		
		convert_to_mp4(outputfilename)