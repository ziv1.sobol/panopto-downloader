# Panopto Downloader
## A python script used to download Panopto Video 


Dillinger is a cloud-enabled, mobile-ready, offline-storage compatible,
AngularJS-powered HTML5 Markdown editor.
In order to download a video from Panopto Do the following things:
1. Open the video you would like to download in your browser and play it
2. Open your browser console and run the following script 
     ```javascript
    var capture_resource = performance.getEntriesByType("resource");
    for (var i = 0; i < capture_resource.length; i++) {
        if (capture_resource[i].initiatorType == "xmlhttprequest" && new RegExp(".*\/sessions\/([0-9a-f\-\/])*.hls\/[0-9]*\/[0-9]*\.ts").test(capture_resource[i].name)) {
            console.log(capture_resource[i].name);
            break;
        }
    }   
    ```
    This script will extract your video download link and print it to the console.
3. run `./download_lesson.py url_from_console`
4. The script will start downloading yur video in parts to a temp directory, and then it'll concat them all to a single .mp4 file.


## Requirements
- python3 with `requests`
- ffmpeg (your can install it using `brew install ffmpeg`)
